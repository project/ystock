Readme
------

Q. What is this?
A. This is a Drupal module which enables you to display stock quotes
   from most national or large stock markets in the world.

Q. How do I install it?
A. Read the INSTALL file 

Q. I have a question. Who do I ask?
A. Chandrashekhar Bhosle <cnb@freedomink.org>
